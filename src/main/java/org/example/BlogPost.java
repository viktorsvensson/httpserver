package org.example;

public class BlogPost {

    private String title;
    private String message;

    public BlogPost(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }
}
