package org.example;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class BlogPostHandler implements HttpHandler {

    List<BlogPost> blogPostList = new ArrayList<>(List.of(
            new BlogPost("Post 1", "Var ute med hunden. Det var kallt."),
            new BlogPost("Glass!", "Idag åt jag glass, äntligen är sommaren här!")
    ));

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        System.out.println(httpExchange.getRequestMethod());

        switch (httpExchange.getRequestMethod()){
            case "GET" -> handleGet(httpExchange);
            case "POST" -> handlePost(httpExchange);
        }
    }

    private void handlePost(HttpExchange httpExchange) throws IOException {
        String reqeustBody = new String(httpExchange.getRequestBody().readAllBytes());

        BlogPost blogPost = new Gson().fromJson(reqeustBody, BlogPost.class);
        blogPostList.add(blogPost);

        System.out.println(reqeustBody);

        httpExchange.sendResponseHeaders(200, 0);
        httpExchange.getResponseBody().close();
    }

    private void handleGet(HttpExchange httpExchange) throws IOException {

        StringBuilder responseHtml = new StringBuilder();

        // Rubrik
        responseHtml
                .append("<!DOCTYPE html><meta charset=\"utf-8\">")
                .append("<h1>Välkommen till bloggen!</h1>");

        // Mappar vi ut blogpost-objekt till blogpostdata i HTML-layout
        blogPostList.forEach(blogPost -> {
            responseHtml
                    .append("<hr />")
                    .append("<h3>" + blogPost.getTitle() + "</h3>")
                    .append("<p>" + blogPost.getMessage() + "</p>");
        });


        // Att skicka tillbaks
        byte[] responseBytes = responseHtml.toString().getBytes(StandardCharsets.UTF_8);
        httpExchange.sendResponseHeaders(200, responseBytes.length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(responseBytes);
        outputStream.close();

    }
}
