package org.example;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

public class Main {

    public static void main(String[] args) throws IOException {

        HttpServer httpServer = HttpServer.create(new InetSocketAddress(8090), 0);

        httpServer.createContext("/blog", new BlogPostHandler());

        httpServer.createContext("/helloworld", httpExchange -> {

            // Strängen som skall tillbaks i bodyn
            String responseHtml = "<!DOCTYPE html><h1>Hello World!</h1>";

            // Bodyn som vi skall skrive HTML-repsonse-strängen till
            OutputStream responseBody = httpExchange.getResponseBody();

            // Omvandlar responseSträngen till bytes (byte[])
            byte[] response = responseHtml.getBytes(StandardCharsets.UTF_8);

            //Sända tillbaks vår respons
            httpExchange.sendResponseHeaders(200, response.length);
            responseBody.write(response);
            responseBody.close();

        });

        httpServer.start();
        System.out.println("Server started...");

    }

}
